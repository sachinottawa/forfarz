"use strict";

const AWS = require("aws-sdk");
AWS.config.update({ region: "ap-south-1" });
const async = require("async");
const { v4: uuidv4 } = require("uuid");
const _ = require("underscore");

const docClient = new AWS.DynamoDB.DocumentClient();

module.exports.createCategories = async (event) => {
  let { title, slug, description, status, coverImage } = event;
  if (!slug) {
    slug = uuidv4();
  }
  let postId = uuidv4();
  let dataId = postId;
  docClient.put(
    {
      TableName: "forfarz-dev-posts",
      Item: {
        post_id: postId,
        data_id: postId,
        title: title,
        slug: slug,
        cover_image: coverImage,
        description: description,
        category_status: status,
        createdOn: new Date().toISOString(),
      },
    },
    (err, data) => {
      if (err) {
        console.log(err);
      } else {
        console.log(data);
        console.log("success");
      }
    }
  );

  return {
    statusCode: 200,
    body: JSON.stringify({
      category: `wow`,
    }),
  };

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};

module.exports.getCategories = async (event, context) => {
  let ExclusiveStartKey;
  let result = await docClient
    .query({
      TableName: "forfarz-dev-posts",
      ExclusiveStartKey,
      IndexName: "status-date-index",
      Limit: 100,
      KeyConditionExpression:
        "post_status = :hashKey and :rangeKey < publishedOn",
      ExpressionAttributeValues: {
        ":hashKey": "published",
        ":rangeKey": "2021-04-23T17:05:51.093Z",
      },
    })
    .promise();
  console.log(`${result} ${ExclusiveStartKey}`);
  return {
    statusCode: 200,
    body: JSON.stringify({
      data: result,
      ExclusiveStartKey: ExclusiveStartKey,
    }),
  };
};
