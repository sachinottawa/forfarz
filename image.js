module.exports.startImageStepFunctionExecution = async (
  event,
  context,
  callback
) => {
  console.log(JSON.stringify(event));
  let filesProcessed = event.Records.map(async (record) => {
    let params = {
      stateMachineArn: process.env.STATE_MACHINE_ARN,
      input: JSON.stringify(record),
    };
    let data = await stepFunctions.startExecution(params).promise();
    console.log(data);
    return data;
  });
  let results = await Promise.all(filesProcessed);
  return results;
};

module.exports.getFileType = async (event, context, callback) => {
  let filename = event.s3.object.key;
  let index = filename.lastIndexOf(".");
  if (index > 0) {
    return filename.substring(index + 1);
  } else {
    return null;
  }
};

const AWS = require("aws-sdk");
AWS.config.update({ region: "ap-south-1" });
const s3 = new AWS.S3();

module.exports.copyFile = async (event, context, callback) => {
  let params = {
    Bucket: process.env.DESTINATION_BUCKET,
    CopySource: encodeURI(
      "/" + event.s3.bucket.name + "/" + event.s3.object.key
    ),
    Key: event.s3.object.key,
  };
  await s3.copyObject(params).promise();
  return {
    region: "ap-south-1",
    bucket: process.env.DESTINATION_BUCKET,
    key: event.s3.object.key,
  };
};

const sharp = require("sharp");

const fs = require("fs");
const os = require("os");
const uuidv4 = require("uuid/v4");
const { promisify } = require("util");
const readFileAsync = promisify(fs.readFile);
const unlinkAsync = promisify(fs.unlink);

AWS.config.update({ region: "us-west-2" });

module.exports.cropImageFile = async (event, context, callback) => {

  fs.readFile("galaxy.jpg", function (err, data) {
    const filename = "galaxy.jpg";
    sharp(data)
      .resize(320, 240)
      .toFormat("jpeg")
      .toBuffer()
      .then((data) => {
        let targetFilename =
          filename.substring(0, filename.lastIndexOf(".")) + "-small.jpg";
        var params = {
          Bucket: process.env.IMAGE_BUCKET,
          Key: targetFilename,
          Body: data,
          ContentType: "image/jpeg",
        };

        console.log(data);
        s3.putObject(params)
          .promise()
          .finally(() => {});
      })
      .catch((err) => {
        console.log(err);
      });
  });

  return {
    region: "us-west-2",
    // bucket: process.env.DESTINATION_BUCKET,
    // key: targetFilename,
  };
};
